import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home-admin',
    component: () => import('../views/HomeAdminView.vue')
  },
  {
    path: '/transactions',
    name: 'transactions-admin',
    component: () => import('../views/TransactionsAdminView.vue')
  },
  {
    path: '/admin',
    name: 'admin',
    component: () => import('../views/AdminView.vue')
  },
  {
    path: '/product',
    name: 'product-admin',
    component: () => import('../views/ProductAdminView.vue')
  },
  {
    path: '/category',
    name: 'category-admin',
    component: () => import('../views/CategoryAdminView.vue')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
